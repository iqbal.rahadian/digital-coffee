package main

import (
	"log"
	"personal/digital-coffee/domain/ingredients/beans"
	"personal/digital-coffee/domain/recipe/coffee"
	"sync"
)

func main() {

	var m sync.Mutex

	beans.InitMutex(&m)

	// create latte function, this part could just be replaced with
	// ay other reciipe
	var latte coffee.Coffee
	latte = coffee.NewLatte()

	if latte.Make() != nil {
		log.Println("failed to make recipe")
	}

}
