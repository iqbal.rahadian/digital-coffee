package coffee

type Coffee interface {
	fetchBeans() error
	fetchMilk() error
	Make() error
}
