package coffee

import (
	"personal/digital-coffee/domain/ingredients/beans"
)

func NewLatte() latte {

	return latte{10, 100}
}

type latte struct {
	beansNeeded int
	milkNeeded  int
}

func (l latte) fetchBeans() error {

	// check if beans are enough to make latte
	err := beans.TakeBeans(l.beansNeeded)
	if err != nil {
		return err
	}

	return nil
}

func (l latte) fetchMilk() error {

	// check if milk are enough to make latte
	// using similar logic to beans

	return nil
}

func (l latte) Make() error {

	err := l.fetchBeans()
	if err != nil {
		return err
	}

	err = l.fetchMilk()
	if err != nil {
		beans.ReturnBeans(l.beansNeeded)
		return err
	}

	// run any logic for making latte here

	return nil
}
