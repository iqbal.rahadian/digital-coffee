package beans

import (
	"errors"
	"sync"
)

var beans int
var restockCount int // used for restocking process
var restockRun bool  // used to make sure no more than one restock process are running
var m *sync.Mutex

func InitMutex(mutex *sync.Mutex) {

	m = mutex

}

func TakeBeans(request int) error {

	m.Lock()
	if beans >= request {
		beans -= request
		m.Unlock()
		return nil
	}
	m.Unlock()

	// run restock when resource are not enough
	Restock()
	return errors.New("beans not enough")

}

func ReturnBeans(beansReturned int) {

	m.Lock()
	beans += beansReturned
	m.Unlock()

}

func Restock() error {

	if restockRun == false {

		// set to true so no other resotck process are run in paralel
		restockRun = true

		var newBeans int

		// logic to restock beans,
		// use mutex to prevent race condition when adding the beans count
		// not when requesting
		m.Lock()
		beans += newBeans
		m.Unlock()

		// set to fal at the end to allow the process called again
		restockRun = false

	}

	return nil

}
